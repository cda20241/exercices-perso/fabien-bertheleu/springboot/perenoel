package com.example.pereNoelSpringboot.controller;

import com.example.pereNoelSpringboot.repository.EnfantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/enfant")
public class EnfantController {

    @Autowired
    private final EnfantRepository enfantRepository;

    public EnfantController(EnfantRepository enfantRepository) {
        this.enfantRepository = enfantRepository;
    }
}
