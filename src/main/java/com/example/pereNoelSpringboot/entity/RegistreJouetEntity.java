package com.example.pereNoelSpringboot.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name="registreJouets")
public class RegistreJouetEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_registre", nullable = false, updatable=false)
    private Long id;

    @Column
    private Date date;

    @OneToMany(mappedBy = "registreJouet", fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<JouetEntity> jouetEntities = new ArrayList<>();

    @OneToMany(mappedBy = "registreJouet", fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<LutinEntity> lutinEntities = new ArrayList<>();

}




