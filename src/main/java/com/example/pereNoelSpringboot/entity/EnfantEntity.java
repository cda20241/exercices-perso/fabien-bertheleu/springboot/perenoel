package com.example.pereNoelSpringboot.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name="enfant")
public class EnfantEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_enfant", nullable = false, updatable=false)
    private Long id;

    @Column private String Nom;
    @Column private Integer Age;
    @Column private String ville;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name="ListeAuPereNoel",
            joinColumns = @JoinColumn(name = "id_enfant"),
            inverseJoinColumns = @JoinColumn(name = "id_jouet"))
    private List<JouetEntity> listeDeJouets = new ArrayList<>();}

