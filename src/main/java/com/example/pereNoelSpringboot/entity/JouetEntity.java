package com.example.pereNoelSpringboot.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="jouet")
public class JouetEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_jouet", nullable = false, updatable = false)
    private Long id;

    @Column
    private String Nom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_registre")
    @JsonBackReference
    private RegistreJouetEntity registreJouet; //


}
