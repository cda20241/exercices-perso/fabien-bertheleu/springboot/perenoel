package com.example.pereNoelSpringboot.repository;

import com.example.pereNoelSpringboot.entity.LutinEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LutinRepository extends JpaRepository<LutinEntity, Long> {
}
