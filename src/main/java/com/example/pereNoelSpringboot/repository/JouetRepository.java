package com.example.pereNoelSpringboot.repository;

import com.example.pereNoelSpringboot.entity.JouetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JouetRepository extends JpaRepository<JouetEntity, Long> {
}
