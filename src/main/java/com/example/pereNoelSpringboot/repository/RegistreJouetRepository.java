package com.example.pereNoelSpringboot.repository;

import com.example.pereNoelSpringboot.entity.RegistreJouetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegistreJouetRepository extends JpaRepository <RegistreJouetEntity, Long> {
}
