package com.example.pereNoelSpringboot.repository;

import com.example.pereNoelSpringboot.entity.EnfantEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnfantRepository extends JpaRepository<EnfantEntity, Long> {
}
